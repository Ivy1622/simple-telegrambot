const TelegramBot = require('node-telegram-bot-api');
const token = '6609853237:AAETcdWdsASyxijNvJLfLkwUZ_yn_x_LAG8';
const bot = new TelegramBot(token, { polling: true });
const stripe = require('stripe')('sk_test_51NgklRFWCZIdXzAezKTacGNrw8BgpWZsdKKkqDnAGg0nQ3dsDhH05j4jLALim7bTkvbxGj5ZebmHUGN6MIqWaYOE00PwWgSM9f');

//Ici, j'envoie un message de bienvenue au client
bot.onText(/\/start/, (msg) => {
  const chatId = msg.chat.id;
  const text = 'Bienvenue dans notre boutique en ligne ! Entrez la commande /products pour consulter la liste de nos produits disponibles';
  bot.sendMessage(chatId, text);
});
bot.onText(/\/products/, (msg) => {
  const chatId = msg.chat.id;
  // Ici, j'envoie une liste de produits au client pour qu'il puisse faire le choix d'un seul produit
  bot.sendMessage(
    chatId,
    'Voici nos produits :\n\n' +
    '1. Ordinateur portable - 500 000 FCFA\n' +
    '2. Iphone 14 pro max - 1 000 000 FCFA\n' +
    '3. Sony PlayStation 4 - 400 000 FCFA\n\n' +
    'Veuillez sélectionner un produit en utilisant les boutons ci-dessous :',
    {
      reply_markup: {
        inline_keyboard: [
          [
            { text: 'Ordinateur portable', callback_data: 'PERSONAL_COMPUTER' },
            { text: 'Iphone 14 pro max', callback_data: 'IPHONE' },
            { text: 'Sony PlayStation 4', callback_data: 'SONY_PS4' },
          ],
        ],
      },
    }
  );
});

// Gestionnaire de callback pour traiter les sélections de produits
bot.on('callback_query', async (callbackQuery) => {
  const chatId = callbackQuery.message.chat.id;
  const selectedProduct = callbackQuery.data;
  let selectedProducts = {}

  let productName = '';
  let productPrice = 0;

  // Récupérer le nom et le prix du produit sélectionné
  if (selectedProduct === 'PERSONAL_COMPUTER') {
    productName = 'Ordinateur portable';
    productPrice = 500000;
  } else if (selectedProduct === 'IPHONE') {
    productName = 'Iphone 14 pro max';
    productPrice = 1000000;
  } else if (selectedProduct === 'SONY_PS4') {
    productName = 'Sony PlayStation 4';
    productPrice = 400000;
  }

  try {
    //pour ajouter ou supprimer un produit de la liste
    if (!selectedProducts[chatId]) {
    selectedProducts[chatId] = [];
    }

    const productIndex = selectedProducts[chatId].indexOf(selectedProduct);

    if (productIndex === -1) {
      selectedProducts[chatId].push(selectedProduct);
    } else {
      selectedProducts[chatId].splice(productIndex, 1);
    }
    // Ici, je crée une session de paiement avec Stripe
    const session = await stripe.checkout.sessions.create({
      payment_method_types: ['card'],
      line_items: [
        {
          price_data: {
            currency: 'xaf',
            product_data: {
              name: productName,
            },
            unit_amount: productPrice,
          },
          quantity: 1,
        },
      ],
      mode: 'payment',
      success_url: 'https://example.com/success',
      cancel_url: 'https://example.com/cancel',
      client_reference_id: chatId.toString(),
    });

    let selectedProductNames = selectedProducts[chatId].map((product) => {
    if (product === 'PERSONAL_COMPUTER') {
      return 'Ordinateur portable';
    } else if (product === 'IPHONE') {
      return 'Iphone 14 pro max';
    } else if (product === 'SONY_PS4') {
      return 'Sony PlayStation 4';
    }
  });
    //Ici, j'affiche le produit sélectionné
    if (selectedProductNames.length > 0) {
      bot.sendMessage(
        chatId,
        'Produit sélectionné :\n\n' + selectedProductNames.join('\n')
      );
    }
    //Ici, j'envoie un lien à l'utilisateur pour qu'il procède au paiement
    await bot.sendMessage(chatId, 'Veuillez cliquer sur le lien ci-dessous pour procéder au paiement :');
    await bot.sendMessage(chatId, session.url);
  } catch (error) {
    console.log(error);
    // Là, c'est pour envoyer un message d'erreur au client en cas de problème
    await bot.sendMessage(chatId, 'Une erreur s\'est produite lors du paiement. Veuillez réessayer ultérieurement.');
  }
});


